#!/bin/fish

#
# Funzioni
#

# Funzione che crea il prompt per read come si desidera

function readPrompt
  printf 'set_color yellow --bold; echo -n "%s"; set_color normal; echo;' $argv;
end

# Controllo che i parametri di input siano dispositivi a blocchi
# e tolgo quelli che non lo sono

set disks;

function checkDisks
end

#
# Script
#

#
# Validazione input
#

set clonezillaImage $argv[1];

set disks;
for argumentIndex in (seq 2 (count $argv))
  if not test -b $argv[$argumentIndex]
    set_color red;
    echo '"'$argv[$argumentIndex]'" non è un dispositivo a blocchi.';
    set_color normal;
  else
    set disks $disks $argv[$argumentIndex];
  end
end
echo;

# Se nessun parametro è giusto, scrivi delle informazioni di utilizzo dello script ed esci
if test \( (count $disks) -eq 0 \) -o \( ! -d $clonezillaImage \)
  echo 'hddTests - Script per testare HDD secondo i fantomatici standard Trashware.';
  echo;
  echo 'Sintassi:  ./hddCloner.fish immagine_clonezilla/ HDD...';
  echo 'Esempio: ./hddCloner.fish ../hdd_images/Vaderetron_2017-10-09-14 /dev/sdb /dev/sdc';
  echo;
  exit;
end

#
# Shredding dischi
#
#
#set_color magenta;
#echo 'Ora sarà avviato lo shredding simultaneo di tutti i dischi specificati. Al termine della procedura premere invio per continuare con la clonazione dell\'immagine.';
#set_color magenta --bold;
#echo -n 'Assolutamente NON PREMERE [Invio] PRIMA DEL TERMINE';
#set_color normal;
#set_color magenta;
#echo ', altrimenti parte la clonazione dei dischi durante il loro sbiancamanto.';
#set_color normal;
#echo;
#read -p (readPrompt 'Premi [Invio] per continuare...');
#echo;
#
#set_color green --bold;
#echo 'Shredding';
#set_color normal;
#echo;
#
## Hack per evitare che il sudo del for successivo mostri il prompt della password
#sudo echo -n;
#
##for disk in $disks
#  sudo shred -v -n 1 $disk &;
#end
#
#read -p '';

#
# Clonazione immagine
#

set_color green --bold;
echo 'Clonazione dell\'immagine';
set_color normal;
echo;

set cloningCommand 'sudo ocs-restore-mdisks -b -p "-g auto -e1 auto -e2 -c -r -j2 -scr -p true" '(string match -r '\/(.*)$' $clonezillaImage)[2];
for disk in $disks
  set cloningCommand $cloningCommand' '(string match -r '\/(\w+)$' $disk)[2]
end
eval $cloningCommand;
